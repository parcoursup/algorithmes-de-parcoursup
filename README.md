La documentation et le code source des algorithmes de Parcoursup sont désormais disponibles à l'adresse [https://gitlab.mim-libre.fr/parcoursup/algorithmes-de-parcoursup](https://gitlab.mim-libre.fr/parcoursup/algorithmes-de-parcoursup) .

